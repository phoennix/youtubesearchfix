<p align = "center">

# YouTube Search Fixer for Firefox, Chrome, Edge, Brave, Vivaldi, Opera and more

### [Official Website](https://phoennix.gitlab.io/youtubesearchfix/) | [Buy me a Coffee](https://www.buymeacoffee.com/ppheonix)  💛

</p>


<details><summary>Changelog</summary>

- **January 23, 2024 - Version 7.0 - Content script changes, added features, misc bug fixes and revamped screenshots.**  
- July 3, 2023 - Version 6.2 - Published to Chrome Web Store, updated script to perform faster, redundant permission removed, added features, misc bug fixes and revamped screenshots.
- October 4, 2022 - Version 5.0 - Added Individual toggles for turning off different kinds of search results. Bug fixes including sidebar height-overflow on smaller screens. Removed option to turn off dynamically-generated video categories which caused random jumps on home page.  
- May 09, 2022 - Version 4.0 - Filter out playlists from search, Remove YouTube mixes from results, Filter out channels from appearing on search results, Disable the YouTube-reels box in search results, Filter out dynamically-generated video categories on home page. Addon approved on Edge Web Store and support for Chromium added.  
- April 17, 2022 -  Version 3.0 - Added options to turn off all customizations. Shorts redirect option added. Lots of misc bug fixes. Added support page.  
- October 2, 2021 - Version 2.0 - Almost complete refactor. New preferences page added to allow users to turn off visual changes. Faster script performance based on receipt of data from server. CSS bugs ironed out. Redundant code removed.  
- August 20, 2021 - Version 1.4.5 - Recommendation snippet "Previously watched" added to filter list, video title's line-height bug on smaller resolution screens fixed, hover over cloud-chip-renderer aka "tags" background-color bug fixed.  
- August 10, 2021 - Line height bug for video titles on small screen displays fixed.  
- June 29, 2021 - Version 1.4.3 - CSS fix for line-height difference in profile names.  
- June 7, 2021 - Version 1.4.2 - CSS classname of video title added.  
- May 30, 2021 - Version 1.4.1 - Minor CSS styling changed to conform to standard YT colors.  
- February 02, 2021 - Version 1.3.4 - Two more recommendation snippets added.  
- December 31, 2020 - Version 1.3.3 - Scrollable tag row color-scheme fixed to conform to the rest of the UI. Bug fixed which rendered selected text invisible. Minor CSS styling changed to conform to standard YT colors.  
- October 13, 2020 - Version 1.2 Another dark pattern card suggestion category "People also searched" now blacklisted. Please update to latest version.  
- August 26, 2020 - Version 1.1 Fixed unloading redundancy added, addon will reload with a DOM reload and click event now.  
- August 18, 2020 - Version 1.0 YouTube's "Learn while you're at home" section will be filtered out.  
- August 11, 2020 - Version 0.91 with anti-clickbait turned off after several requests. Bug with subscription page dissapearing fixed.  
- August 7, 2020: Version 0.8 with trending Icons and YouTuber Posts removed. Minor bug which caused 'Sort By' to dissappear fixed.  
- August 7, 2020: Version 0.7 with bug regarding light mode title color fixed.  
- August 6, 2020: Version 0.6 released. Top navigation bar is now lower opacity unless you hover on it.  
- August 5, 2020: Version 0.5 with anti-clickbait title and submit-button bug fix released.  
- July 31, 2020: Version 0.4 with visual fixes, added sidebar highlight released.  
- July 21, 2020: Version 0.3 with light mode support released.  
- July 20, 2020: Version 0.2 with even more search distraction element removal is now live.  
</details>

---


<strong>This extension is designed to fix YouTube search, redirect shorts and add optional visual changes. Features include: </strong>

1. Removes suggestions like "For you", "People also search for", "Searches related to", "Learn while you're at home", "Related to your search", "Learn while you're at home", "People also watched", "Other people are watching", "Recommended to you" and "Latest YouTube posts" from your feed.  
2. Remove Shorts from search results.
3. Remove Live Videos from search results.
4. Highlights the current tag selected.
5. An options page to turn off visual changes.
6. More spaced out collapsed sidebar on homepage with accent color to signify active tab.
7. Video Spolight (changeable).
8. Unified font face across the website.
9. Bigger centered video title (changeable)
10. Redirect shorts back to legacy page.
11. Turn off playlists from appearing in search results.
12. Hide YouTube mixes and reels suggestions.
13. Filter out channel listings from results.




---

> _Please note this extension works on both Light and Dark Mode. YouTube™ and Google™ are trademarks of Google Inc. Use of these trademarks is subject to Google Permissions._
