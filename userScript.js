// ==UserScript==
// @name        YouTube Search Fixer
// @description Add to remove distracting search suggestions
// @license     https://creativecommons.org/licenses/by-sa/4.0/
// @version     2.0
// @author      https://gitlab.com/phoennix
// @run-at      document-idle
// @homepageURL https://gitlab.com/phoennix/youtubesearchfix
// @match        https://www.youtube.com/results?search_query=*
// @icon         https://www.google.com/s2/favicons?domain=youtube.com
// @grant        none
// ==/UserScript==

(function () {
  "use strict";
  fiii();
  function fiii() {
    const observer = new PerformanceObserver((list) => {
      for (const entry of list.getEntries()) {
        if (entry.initiatorType === "fetch" || entry.initiatorType == "img") {
          removeDistractions();
        }
      }
    });
    observer.observe({ entryTypes: ["resource"] });
    fetch("youtube.com")
      .then((res) => res.text())
      .then((text) => console.log(text.split("\n")[0]));
  }
  function removeDistractions() {
    var related = document.querySelectorAll("#title");
    var related1 = document.querySelectorAll(
      ".style-scope.yt-formatted-string"
    );
    var searchText1 = "Related to your search";
    var searchText2 = "For you";
    var searchText3 = "People also search for";
    var searchText4 = "Searches related to ";
    var searchText5 = "Learn while you're at home";
    var searchText6 = "People also watched";
    var searchText7 = "Other people are watching";
    var searchText8 = "Recommended to you";
    var searchText9 = "Previously watched";
    var posts = "Latest YouTube posts";
    var found1,
      found2,
      found3,
      found4,
      found5,
      found7,
      found8,
      found9,
      foundPosts;

    let r2 = document.querySelectorAll(".clarify-box");

    for (let f2 = 0; f2 < r2.length; f2++) r2[f2].remove();

    for (var i = 0; i < related.length; i++) {
      if (related[i].textContent == searchText1) {
        found1 = related[i];
        found1.parentElement.parentElement.parentElement.parentElement.remove();
      } else if (related[i].textContent == searchText2) {
        found2 = related[i];
        found2.parentElement.parentElement.parentElement.parentElement.remove();
      } else if (related[i].textContent == searchText3) {
        found3 = related[i];
        found3.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.remove();
      } else if (related[i].textContent == searchText5) {
        found5 = related[i];
        found5.parentElement.parentElement.parentElement.parentElement.remove();
      } else if (related[i].textContent == posts) {
        foundPosts = related[i];
        foundPosts.parentElement.parentElement.parentElement.parentElement.parentElement.remove();
      } else if (related[i].textContent == searchText6) {
        foundPosts = related[i];
        foundPosts.parentElement.parentElement.parentElement.parentElement.parentElement.remove();
      } else if (related[i].textContent == searchText7) {
        found7 = related[i];
        found7.parentElement.parentElement.parentElement.parentElement.remove();
      } else if (related[i].textContent == searchText8) {
        found8 = related[i];
        found8.parentElement.parentElement.parentElement.parentElement.remove();
      } else if (related[i].textContent == searchText9) {
        found9 = related[i];
        found9.parentElement.parentElement.parentElement.parentElement.parentElement.remove();
      }
    }

    for (var j = 0; j < related1.length; j++) {
      let temp = related1[j].textContent;
      if (temp == searchText4) {
        found4 = related1[j];
        found4.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.style.display =
          "none";
      }
    }
  }
})();
